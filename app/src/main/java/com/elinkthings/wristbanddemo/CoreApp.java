package com.elinkthings.wristbanddemo;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;


import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;


/**
 *
 */
public class CoreApp extends Application implements LifecycleObserver {
    public static final String CHANNEL_ID = "com.elinkthings.wristbanddemo.channel.bleservice";
    public static final String CHANNEL_ID1 = "com.elinkthings.wristbanddemo.ble.notification";
    private static CoreApp instance;

    public BLEScanner bleScanner = null;
//
//    public BLE_Becon_Advertiser bleAdvertiser = null;
//    public BLE_Beacon_Sanner bleScanner = null;

    private static Boolean isAppForgraouond = false;




 


    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static synchronized CoreApp getInstance() {
        return instance;
    }


    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static Boolean getIsAppForground() {
        return isAppForgraouond;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //MultiDex.install(this);
        //printHashKey(this);
    }

    

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Ble service channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);

            NotificationChannel serviceChannel1 = new NotificationChannel(
                    CHANNEL_ID1,
                    "Ble other service channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager1 = getSystemService(NotificationManager.class);
            manager1.createNotificationChannel(serviceChannel);
            NotificationChannel serviceDefaultChannel = new NotificationChannel(
                    getString(R.string.default_notification_channel_id),
                    getString(R.string.default_notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(serviceDefaultChannel);
            // Create channel to show notifications.

        }


    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        

        createNotificationChannel();

       
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    



    

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        //App in background

        isAppForgraouond = false;
        Log.e("App","onStop");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        // App in foreground
        isAppForgraouond = true;
        Log.e("App","onStart");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onAppResume() {
        // App in foreground
        isAppForgraouond = true;
        Log.e("App","ON_RESUME");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onStoped(){
        Log.e("App","onDestroy");
    }
}