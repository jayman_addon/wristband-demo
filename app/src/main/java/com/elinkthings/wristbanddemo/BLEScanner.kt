package com.elinkthings.wristbanddemo

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothGatt
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.elinkthings.distrackerlibrary.HealthBraceletBleConfig
import com.elinkthings.wristbanddemo.utils.ScanUtil
import com.pingwang.bluetoothlib.bean.BleValueBean
import com.pingwang.bluetoothlib.listener.CallbackDisIm
import com.pingwang.bluetoothlib.listener.OnCallbackBle
import com.pingwang.bluetoothlib.listener.OnScanFilterListener
import com.pingwang.bluetoothlib.server.ELinkBleServer
import com.pingwang.bluetoothlib.server.ELinkBleServer.BluetoothBinder
import com.pingwang.bluetoothlib.utils.BleStrUtils
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class BLEScanner(val context: Context, adapter: BluetoothAdapter):OnCallbackBle, OnScanFilterListener {

    private var bindIntent: Intent? = null
    private var mBluetoothService: ELinkBleServer? = null

    companion object {
        private const val TAG = "BluetoothLeScanner"
        private const val TAG1 = "YYYYYYLeScanner"
      //  var mDeviseMap = HashMap<String, MyScanDevice>()
        // val handler = Handler()
    }

    // Eddystone frame types
    private val TYPE_UID: Byte = 0x00
    private val TYPE_URL: Byte = 0x10
    private val TYPE_TLM: Byte = 0x20
    val UID_SERVICE_Beacon =
        java.util.UUID.fromString("0000feaa-0000-1000-8000-00805f9b34fb")
    var bluetoothGatt: BluetoothGatt? = null

    var isScanning: Boolean = false

    private var handler = Handler()



    @RequiresApi(api = Build.VERSION_CODES.M)
    fun startScanning(serviceUUIDs: Array<UUID>?) {
        if (isScanning) return

        bindService()
        if (mBluetoothService != null) {
            mBluetoothService!!.scanLeDevice(0, HealthBraceletBleConfig.UUID_SERVER)
         Log.e("in","mBluetoothService !null")
        }else{
            Log.e("in","mBluetoothService == null")

        }

        if (mBluetoothService != null) {
            mBluetoothService!!.setOnCallback(callbacks)
            mBluetoothService!!.setOnScanFilterListener(filterListener)
        }

        // Bug workaround: Restart periodically so the Bluetooth daemon won't get into a borked state
        handler.removeCallbacksAndMessages(null)
        handler.postDelayed({
            if (isScanning) {
                Log.e(TAG, "Restarting scan...")
                //stopScanning()
                //startScanning(serviceUUIDs)
            }
        }, TimeUnit.SECONDS.toMillis(5))


    }



    fun stopScanning() {
        if (!isScanning) return

        try {
            isScanning = false
            if (mBluetoothService != null) {
                mBluetoothService!!.stopScan()
            }
            Log.i(TAG, "Stopped scan")
        } catch (exception: Exception) {
            Log.e(TAG, "Stop scan failed: $exception")
        }
    }

    //---------------------------------服务---------------------------------------------------
    private fun bindService() {
        if (bindIntent == null) {
            bindIntent = Intent(context, ELinkBleServer::class.java)
            if (mFhrSCon != null){
                try {
                    Log.e("in","mFhrSCon != null")

                    context.bindService(bindIntent, mFhrSCon, Context.BIND_AUTO_CREATE)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }else{
                Log.e("in","mFhrSCon == null")

            }
        }
    }

    private val mFhrSCon: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            try {
                mBluetoothService = (service as BluetoothBinder).service
                if (mBluetoothService != null) {
                    Log.e("in","mBluetoothService~ != null")

                    mBluetoothService!!.setOnCallback(callbacks)
                    mBluetoothService!!.setOnScanFilterListener(filterListener)
                  // mHandler.sendEmptyMessage(BIND_SERVER_OK)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mBluetoothService = null
        }
    }
    public fun unbindService() {
        CallbackDisIm.getInstance().removeListener(callbacks)
        if (mFhrSCon != null)
            context.unbindService(mFhrSCon)
        bindIntent = null
    }


    fun refreshData() {
        if (isScanning) {}


    }



    fun Double.roundTo(n: Int): Double {
        return "%.${n}f".format(this).toDouble()
    }

    fun getDateromNanoSecond(longNano: Long): String {
        val millis = TimeUnit.MILLISECONDS.convert(longNano, TimeUnit.NANOSECONDS)
        val formatter: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        val dateString = formatter.format(Date(millis))
        return dateString
    }
    val callbacks=object : OnCallbackBle {
        override fun onStartScan() {
            super.onStartScan()
        }
        override fun onScanning(data: BleValueBean?) {
            super.onScanning(data)
            Log.e("onscanning", data.toString())
            val mAddress = data?.mac
            //        if (!mList.contains(mAddress + "=" + data.getName())) {
//            mList.add(mAddress + "=" + data.getName());
//            listAdapter.notifyDataSetChanged();
//
//        }

            val manufacturerDataList = data!!.manufacturerDataList
            if (manufacturerDataList == null || manufacturerDataList.isEmpty()) {
                return
            }
            val data1 = manufacturerDataList[0]
            var data2: ByteArray? = null
            if (manufacturerDataList.size > 1) {
                data2 = manufacturerDataList[1]
            }

            Log.e(TAG, "设备地址||厂商数据:" + data.mac + "||" + BleStrUtils.byte2HexStr(data1) + "||" + ScanUtil().getMacAddress(data2).toUpperCase())

        }

        override fun onDisConnected(mac: String?, code: Int) {
            super.onDisConnected(mac, code)
            Toast.makeText(context, "Disconnect:$code", Toast.LENGTH_SHORT).show()

        }

        override fun onServicesDiscovered(mac: String?) {
            super.onServicesDiscovered(mac)
        }

        override fun bleClose() {
            super.bleClose()
            Toast.makeText(context, "Bluetooth is not turned on", Toast.LENGTH_SHORT).show()

        }

        override fun bleOpen() {
            super.bleOpen()
        }


        override fun onConnecting(mac: String?) {
            super.onConnecting(mac)
        }



        override fun onScanTimeOut() {
            super.onScanTimeOut()
        }
    }

    val filterListener=object : OnScanFilterListener {
        override fun onFilter(bleValueBean: BleValueBean?): Boolean {
            Log.e("in", "onFilter" + bleValueBean.toString())

            return super.onFilter(bleValueBean)
        }

        override fun onScanRecord(bleValueBean: BleValueBean?) {
            super.onScanRecord(bleValueBean)
            Log.e("in", "onScanRecord" + bleValueBean.toString())

        }
    }


    fun onresume(){
                if (mBluetoothService != null) {
            mBluetoothService!!.setOnCallback(callbacks);
            mBluetoothService!!.setOnScanFilterListener(filterListener);
        }
    }
}

