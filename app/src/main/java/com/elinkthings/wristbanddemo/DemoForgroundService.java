package com.elinkthings.wristbanddemo;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.elinkthings.distrackerlibrary.HealthBraceletBleConfig;
import com.pingwang.bluetoothlib.bean.BleValueBean;
import com.pingwang.bluetoothlib.listener.CallbackDisIm;
import com.pingwang.bluetoothlib.listener.OnCallbackBle;
import com.pingwang.bluetoothlib.listener.OnScanFilterListener;
import com.pingwang.bluetoothlib.server.ELinkBleServer;
import com.pingwang.bluetoothlib.utils.BleLog;
import com.pingwang.bluetoothlib.utils.BleStrUtils;

import java.util.List;
import java.util.Timer;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class DemoForgroundService extends Service {
    private Intent bindIntent = null;
    private  ELinkBleServer mBluetoothService= null;
    // APP
    private CoreApp app = null;
    private Timer  timer  = null;
    private final String CHANNEL_ID = "wristbanddemoe_alert_Channel";
    private final String TAG = "BLEForegroundService";
    private final String TAG_DURATION="Duration";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();


        Intent mUserService = new Intent(this.getApplicationContext(), ELinkBleServer.class);
        startService(mUserService);

        bindService();
        IntentFilter filter1 =new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver1, filter1);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getAction().equals(BLECONSANT.INSTANCE.getSTARTFOREGROUND_ACTION())) {
            Log.i(TAG, "Received Start Foreground Intent ");

            startBLe();

        }  else if (intent.getAction().equals(BLECONSANT.INSTANCE.getSTOPFOREGROUND_ACTION())) {
            Log.i(TAG, "Received Stop Foreground Intent");
            if (mBluetoothService != null) {
                mBluetoothService.stopScan();
            }

            stopForeground(true);
            stopSelf();
        }
        return Service.START_NOT_STICKY;
    }
    @RequiresApi(Build.VERSION_CODES.M)
    private void startBLe() {
        createNotificationChannelIfNeeded();

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent =  PendingIntent.getActivity(
                this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT
        );

        Intent playIntent = new Intent(this,DemoForgroundService.class);
        playIntent.setAction(BLECONSANT.INSTANCE.getSTOPFOREGROUND_ACTION());

        PendingIntent pplayIntent = PendingIntent.getService(
                this, 12345,
                playIntent, 0
        );

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(
                       new NotificationCompat.BigTextStyle()
                                .bigText("This app utilizes Bluetooth signal to measure the distance between two phones")
                )
                .setContentText("")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setCategory(Notification.CATEGORY_SERVICE)
                .addAction(
                        android.R.drawable.ic_menu_close_clear_cancel, "STOP",
                        pplayIntent
                )
                // .addAction(R.drawable.ic_close,"Close",pendingIntentYes)
                .build();
        startForeground(6, notification);
        if (mBluetoothService != null) {

            mBluetoothService.scanLeDevice(0, HealthBraceletBleConfig.UUID_SERVER);
            Log.e("in","mBluetoothService !null");

        }else{
                Log.e("in","mBluetoothService == null");

            }



    }
    @Override
    public void onDestroy() {
        unbindService();
        if(mBroadcastReceiver1!=null){
            unregisterReceiver(mBroadcastReceiver1);
        }
        super.onDestroy();
    }



    /**
     * This notification channel is only required for android versions above
     * android O. This creates the necessary notification channel for the foregroundService
     * to function.
     */
    private void createNotificationChannelIfNeeded() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);

        }
    }

    private void bindService() {
        if (bindIntent == null) {
            bindIntent = new Intent(getApplicationContext(), ELinkBleServer.class);
            if (mFhrSCon != null){
                try {
                    Log.e("in","mFhrSCon != null");

                    this.bindService(bindIntent, mFhrSCon, Context.BIND_AUTO_CREATE);
                } catch ( Exception e) {
                    e.printStackTrace();
                }

            }else{
                Log.e("in","mFhrSCon == null");

            }
        }
    }

    private void unbindService() {
        CallbackDisIm.getInstance().removeListener(callbackBle);
        if (mFhrSCon != null)
            this.unbindService(mFhrSCon);
        bindIntent = null;
    }
    private ServiceConnection mFhrSCon = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBluetoothService = ((ELinkBleServer.BluetoothBinder) service).getService();
            if (mBluetoothService != null) {
                mBluetoothService.setOnCallback(callbackBle);
                mBluetoothService.setOnScanFilterListener(filterListener);

            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBluetoothService = null;
        }
    };

    OnCallbackBle callbackBle =new OnCallbackBle() {
        @Override
        public void onStartScan() {

        }

        @Override
        public void onScanning(BleValueBean data) {
            Log.e("onscanning",data.toString());
            String mAddress = data.getMac();



            List<byte[]> manufacturerDataList = data.getManufacturerDataList();
            if (manufacturerDataList == null || manufacturerDataList.isEmpty()) {
                return;
            }
            byte[] data1 =manufacturerDataList.get(0);
            byte[] data2 =null;

            if (manufacturerDataList.size()>1){
                data2=manufacturerDataList.get(1);
            }
            String s=mAddress + "=" + data.getName()+"\ndata="+ BleStrUtils.byte2HexStr(data1)+ "||" + getMacAddress(data2).toUpperCase();


            BleLog.i(TAG, "设备地址||厂商数据:" + data.getMac() + "||" + BleStrUtils.byte2HexStr(data1) + "||" + getMacAddress(data2).toUpperCase());

        }



        @Override
        public void onConnecting(String mac) {

        }

        @Override
        public void onDisConnected(String mac, int code) {
            Toast.makeText(DemoForgroundService.this, "Disconnect:" + code, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServicesDiscovered(String mac) {

        }

        @Override
        public void bleOpen() {

        }

        @Override
        public void bleClose() {
            Toast.makeText(DemoForgroundService.this, "Bluetooth is not turned on", Toast.LENGTH_SHORT).show();

        }
    };

    OnScanFilterListener filterListener =new OnScanFilterListener() {
        @Override
        public boolean onFilter(BleValueBean bleValueBean) {
            Log.e("in", "onFilter" + bleValueBean.toString());
            return true;
        }

        @Override
        public void onScanRecord(BleValueBean bleValueBean) {
            Log.e("in", "onScanRecord" + bleValueBean.toString());
        }
    };



    private String getMacAddress(byte[] macByte){
        StringBuilder hs = new StringBuilder();
        for(int i = macByte.length-1; i >=0;i--) {
            byte aB = macByte[i];
            int a = aB & 0xFF;
            String stmp = Integer.toHexString(a);
            if (stmp.length() == 1) {
                hs.append("0").append(stmp);
            } else {
                hs.append(stmp);
            }
            hs.append(":");
        }
        if (hs.length()>0)
            hs.deleteCharAt(hs.length()-1);
        return hs.toString();
    }


    BroadcastReceiver mBroadcastReceiver1 =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                String action = intent.getAction();
                if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                    int state =
                            intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                    switch (state) {
                        case BluetoothAdapter.STATE_OFF:
                            Log.e("BLE STATE", "STATE_OFF");
                            //  Toaster.longToast("App is Not Work Without  Bluetooth")

                            stopSelf();
                        break;
                        case BluetoothAdapter.STATE_TURNING_OFF :
                            Log.e("BLE STATE", "STATE_TURNING_OFF");
                            //  Toaster.longToast("App is Not Work Without  Bluetooth")

                            stopSelf();
                            break;



                    }
                }
            }
        }
    };
}
