package com.elinkthings.wristbanddemo.utils;

public class ScanUtil {
    public String getMacAddress(byte[] macByte){
        StringBuilder hs = new StringBuilder();
        for(int i = macByte.length-1; i >=0;i--) {
            byte aB = macByte[i];
            int a = aB & 0xFF;
            String stmp = Integer.toHexString(a);
            if (stmp.length() == 1) {
                hs.append("0").append(stmp);
            } else {
                hs.append(stmp);
            }
            hs.append(":");
        }
        if (hs.length()>0)
            hs.deleteCharAt(hs.length()-1);
        return hs.toString();
    }
}
