package com.elinkthings.wristbanddemo

import android.app.*
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.elinkthings.distrackerlibrary.HealthBraceletBleConfig
import com.elinkthings.wristbanddemo.utils.ScanUtil
import com.pingwang.bluetoothlib.bean.BleValueBean
import com.pingwang.bluetoothlib.listener.CallbackDisIm
import com.pingwang.bluetoothlib.listener.OnCallbackBle
import com.pingwang.bluetoothlib.listener.OnScanFilterListener
import com.pingwang.bluetoothlib.server.ELinkBleServer
import com.pingwang.bluetoothlib.utils.BleStrUtils
import java.util.*
import java.util.concurrent.TimeUnit


class BLEForegroundService : Service() {
    private var bindIntent: Intent? = null
    private var mBluetoothService: ELinkBleServer? = null
    // APP
    private var app: CoreApp? = null
    private var timer: Timer? = null
    companion object {
        // CONSTANTS
        private const val CHANNEL_ID = "wristbanddemoe_alert_Channel"
        private const val TAG = "BLEForegroundService"
        private const val TAG_DURATION="Duration"


    }

    override fun onCreate() {
        super.onCreate()
        val application = (application as? CoreApp) ?: return
        app = application
        val adapter = BluetoothAdapter.getDefaultAdapter()


        app?.bleScanner = BLEScanner(this, adapter)
        val mUserService = Intent(this, ELinkBleServer::class.java)
        startService(mUserService)
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                 IntentFilter("custom-event-name12"))
        bindService()
    }


    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (mBluetoothService != null) {
            mBluetoothService?.setOnCallback(callbacks);
            mBluetoothService?.setOnScanFilterListener(filterListener);
        }

        if (intent?.action.equals(BLECONSANT.STARTFOREGROUND_ACTION)) {
            Log.i(TAG, "Received Start Foreground Intent ")

            startBLe(true)

        } else if (intent?.action.equals(BLECONSANT.YES_ACTION)) {
            Log.i(TAG, "Clicked Previous")
            startBLe(false)


        } else if (intent?.action.equals(BLECONSANT.STOPFOREGROUND_ACTION)) {
            Log.i(TAG, "Received Stop Foreground Intent")
            val intent = Intent("custom-event-name")
            // You can also include some extra data.
            // You can also include some extra data.
            intent.putExtra("isOff", true)
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

            stopForeground(true)
            stopSelf()
        } else if (intent?.action.equals("RestartAdd")) {
            Log.i(TAG, "RestartAdd")
            //startBLe1(true)
           
        }


        return Service.START_NOT_STICKY
    }



    @RequiresApi(Build.VERSION_CODES.M)
    private fun startBLe(b: Boolean) {
        createNotificationChannelIfNeeded()

        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent = PendingIntent.getActivity(
            this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )
        val yesReceive = Intent()
        yesReceive.action = BLECONSANT.YES_ACTION


        val playIntent = Intent(this, BLEForegroundService::class.java)
        playIntent.action = BLECONSANT.STOPFOREGROUND_ACTION
        val pplayIntent = PendingIntent.getService(
            this, 12345,
            playIntent, 0
        )
        val pendingIntentYes =
            PendingIntent.getBroadcast(this, 12345, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT)
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(getString(R.string.app_name))
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText("This app utilizes Bluetooth signal to measure the distance between two phones")
            )
            .setContentText("")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .setCategory(Notification.CATEGORY_SERVICE)
            .addAction(
                android.R.drawable.ic_menu_close_clear_cancel, "STOP",
                pplayIntent
            )
            // .addAction(R.drawable.ic_close,"Close",pendingIntentYes)
            .build()
        startForeground(6, notification)
        if (b) {
            if (mBluetoothService != null) {
                mBluetoothService!!.scanLeDevice(0, HealthBraceletBleConfig.UUID_SERVER)
                Log.e("in","mBluetoothService !null")
            }else{
                Log.e("in","mBluetoothService == null")

            }

            //home mode on
            // scheduler a new timer to start changing the contact event numbers
            //app?.bleScanner?.stopScanning()
//            timer?.cancel()
//            timer = Timer()
//            timer?.scheduleAtFixedRate(
//                object : TimerTask() {
//                    override fun run() {
//
//                      app?.bleScanner?.refreshData()
//                    }
//                },
//                TimeUnit.MILLISECONDS.toMillis(120.toLong()),
//                TimeUnit.MILLISECONDS.toMillis(120.toLong())
//            )
//
//            // val newContactEventUUID = UUID.fromString(PreferanceUtils.getUUID(this, "", "uuid"))
//            val newContactEventUUID = UUID.fromString("116ce28b-c141-4ae7-b68b-beda160a700a")
//
////116ce28b-c141-4ae7-b68b-beda160a700a
//            val mUserService = Intent(this, ELinkBleServer::class.java)
//            startService(mUserService)
//            app?.bleScanner?.startScanning(arrayOf<UUID>(BLECONSANT.CONTACT_EVENT_SERVICE))
        } else {
            if (mBluetoothService != null) {
                mBluetoothService!!.stopScan()
            }
            //home mode off
//            app?.bleScanner?.stopScanning()
//            timer?.apply {
//                cancel()
//                purge()
//            }
//            // val newContactEventUUID = UUID.fromString(PreferanceUtils.getUUID(this, "", "uuid"))
//            val newContactEventUUID = UUID.fromString("116ce28b-c141-4ae7-b68b-beda160a700a")

//116ce28b-c141-4ae7-b68b-beda160a700a
            

        }

    }

    override fun onDestroy() {
        Log.e("in", "ondestroy service" + CoreApp.getInstance() + "<")
        closeAll()
        Log.e(TAG_DURATION,"onDestroy")

        val intent = Intent("home-broadcast-close")
        // You can also include some extra data.
        // You can also include some extra data.
        intent.putExtra("isOff", true)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        unbindService()
        super.onDestroy()


    }


    fun closeAll() {

       // BLEScanner.mDeviseMap.clear()
        //BLE_Beacon_Sanner.mDeviseMap.clear()

       // app?.bleScanner?.stopScanning()
       // app?.bleScanner?.unbindService()
        timer?.apply {
            cancel()
            purge()
        }
        mBroadcastReceiver1?.let {
//            unregisterReceiver(mBroadcastReceiver1)

        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)

    }


    /**
     * This notification channel is only required for android versions above
     * android O. This creates the necessary notification channel for the foregroundService
     * to function.
     */
    private fun createNotificationChannelIfNeeded() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(serviceChannel)
        }
    }


    private val mBroadcastReceiver1 = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent != null && intent.action != null) {
                val action = intent.action
                if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                    val state =
                        intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)
                    when (state) {
                        BluetoothAdapter.STATE_OFF -> {
                            Log.e("BLE STATE", "STATE_OFF")
                            //  Toaster.longToast("App is Not Work Without  Bluetooth")
                            val intent = Intent("custom-event-name")
                            // You can also include some extra data.
                            // You can also include some extra data.
                            intent.putExtra("isOff", true)
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                            stopSelf()
                        }
                        BluetoothAdapter.STATE_TURNING_OFF -> {
                            Log.e("BLE STATE", "STATE_TURNING_OFF")
                            //  Toaster.longToast("App is Not Work Without  Bluetooth")
                            val intent = Intent("custom-event-name")
                            // You can also include some extra data.
                            // You can also include some extra data.
                            intent.putExtra("isOff", true)
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                            stopSelf()

                        }
                        BluetoothAdapter.STATE_ON -> {
                            Log.e("BLE STATE", "STATE_ON")

                        }
                        BluetoothAdapter.STATE_TURNING_ON -> {
                            Log.e("BLE STATE", "STATE_TURNING_ON")


                        }
                    }
                }
            }

        }
    }


    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val message = intent.getStringExtra("message")
            if(message.equals("his is my message!")){
                if (mBluetoothService != null) {
                    mBluetoothService!!.setOnCallback(callbacks);
                    mBluetoothService!!.setOnScanFilterListener(filterListener);
                }
            }
            Log.d("receiver", "Got message: $message")
        }
    }


    private fun bindService() {
        if (bindIntent == null) {
            bindIntent = Intent(this, ELinkBleServer::class.java)
            if (mFhrSCon != null){
                try {
                    Log.e("in","mFhrSCon != null")

                    this.bindService(bindIntent, mFhrSCon, Context.BIND_AUTO_CREATE)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }else{
                Log.e("in","mFhrSCon == null")

            }
        }
    }
    private val mFhrSCon: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            try {
                mBluetoothService = (service as ELinkBleServer.BluetoothBinder).service
                if (mBluetoothService != null) {
                    Log.e("in","mBluetoothService~ != null")

                    mBluetoothService!!.setOnCallback(callbacks)
                    mBluetoothService!!.setOnScanFilterListener(filterListener)
                    // mHandler.sendEmptyMessage(BIND_SERVER_OK)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mBluetoothService = null
        }
    }
    public fun unbindService() {
        CallbackDisIm.getInstance().removeListener(callbacks)
        if (mFhrSCon != null)
            this.unbindService(mFhrSCon)
        bindIntent = null
    }

    val callbacks=object : OnCallbackBle {
        override fun onStartScan() {
            super.onStartScan()
        }
        override fun onScanning(data: BleValueBean?) {
            super.onScanning(data)
            Log.e("onscanning", data.toString())
            val mAddress = data?.mac
            //        if (!mList.contains(mAddress + "=" + data.getName())) {
//            mList.add(mAddress + "=" + data.getName());
//            listAdapter.notifyDataSetChanged();
//
//        }

            val manufacturerDataList = data!!.manufacturerDataList
            if (manufacturerDataList == null || manufacturerDataList.isEmpty()) {
                return
            }
            val data1 = manufacturerDataList[0]
            var data2: ByteArray? = null
            if (manufacturerDataList.size > 1) {
                data2 = manufacturerDataList[1]
            }

            Log.e("BLEScanner.TAG", "设备地址||厂商数据:" + data.mac + "||" + BleStrUtils.byte2HexStr(data1) + "||" + ScanUtil().getMacAddress(data2).toUpperCase())

        }

        override fun onDisConnected(mac: String?, code: Int) {
            super.onDisConnected(mac, code)
            Toast.makeText(this@BLEForegroundService, "Disconnect:$code", Toast.LENGTH_SHORT).show()

        }

        override fun onServicesDiscovered(mac: String?) {
            super.onServicesDiscovered(mac)
        }

        override fun bleClose() {
            super.bleClose()
            Toast.makeText(this@BLEForegroundService, "Bluetooth is not turned on", Toast.LENGTH_SHORT).show()

        }

        override fun bleOpen() {
            super.bleOpen()
        }


        override fun onConnecting(mac: String?) {
            super.onConnecting(mac)
        }



        override fun onScanTimeOut() {
            super.onScanTimeOut()
        }
    }

    val filterListener=object : OnScanFilterListener {
        override fun onFilter(bleValueBean: BleValueBean?): Boolean {
            Log.e("in", "onFilter" + bleValueBean.toString())

            return super.onFilter(bleValueBean)
        }

        override fun onScanRecord(bleValueBean: BleValueBean?) {
            super.onScanRecord(bleValueBean)
            Log.e("in", "onScanRecord" + bleValueBean.toString())

        }
    }
}