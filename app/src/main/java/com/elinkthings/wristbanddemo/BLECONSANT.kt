package com.elinkthings.wristbanddemo

import android.os.ParcelUuid
import java.util.*

object BLECONSANT {
    val YES_ACTION: String = "ACTIION_YES_CLOSE"
    val UUID = ParcelUuid.fromString("0000FED8-0000-1000-8000-00805F9B34FB")

    val SERVICE_UUID =
        java.util.UUID.fromString("00001111-0000-1000-8000-00805F9B34FB")
    val CHAR_UUID =
        java.util.UUID.fromString("00002222-0000-1000-8000-00805F9B34FB")


    /// The string representation of the UUID for the primary peripheral service
//    var CONTACT_EVENT_SERVICE: UUID =
//        java.util.UUID.fromString("57A0ED25-A076-431F-974B-71949A16A3E9")
    var CONTACT_EVENT_SERVICE: UUID =
        java.util.UUID.fromString("0000B017-0000-1000-8000-00805F9B34FB")

    /// The string representation of the UUID for the contact event identifier characteristic
    //sam-d5a8cc51-5b56-4a9e-af7f-a5432cf1d9f2
    //asus-87676934-75df-453b-b7a5-5d01292d9280
    var CONTACT_EVENT_IDENTIFIER_CHARACTERISTIC: UUID =
        java.util.UUID.fromString("8C075C56-05A6-4C10-B33A-10CF6571725F")


    val STARTFOREGROUND_ACTION =
        "com.apps.social_distance_alert.foregroundservice.action.startforeground"
    val STOPFOREGROUND_ACTION =
        "com.apps.social_distance_alert.foregroundservice.action.stopforeground"

    val alertDistanceInHand = "4.8"
    val alertDistanceInPocket = "5.7"
    val NAMESPACE = "ED81237BCEE036A43B3F"

    //
    enum class RXPOWERLEVEL(val toint: Int) {
        RX1(-63),
        RX2(-65),

        RX3(-67),
        RX4(-69),
        RX5(-71),


    }

    enum class BLEDistance(val toString: String) {

        SAFE("safe"),
        PHONE_ALERT("phoneAlert"),
//        //6 ft
//        BEACON_ALERT("beaconAlert"),
//
//        //12ft
//        BEACON_NEARBY("beaconNearBy"),

    }


    enum class BLE_ALERT_TYPE(val toString: String) {

        NORMAL("NORMAL"),

//        //6 ft
//        SIXFEET("SIXFEET"),
//
//        //12ft
//        TENFEET("TENFEET"),

    }
}